#!/usr/bin/env python3
# coding: utf-8
import click
import mimetypes
import os
import requests


def nextcloud_uploader(server, username, password, base_path):

    def upload_file(filename):
        mimetype, _encoding = mimetypes.guess_type(filename)
        _path, fname = os.path.split(filename)
        url = f"{server}/remote.php/dav/files/{username}/{base_path}/{fname}"
        headers = {"Content-type": mimetype}
        click.echo(f"uploading {fname} (mimetype: {mimetype})")
        response = requests.put(
            url,
            auth=(username, password),
            headers=headers,
            data=open(filename, "rb"),
        )
        if response.status_code != 201:
            click.echo(f"HTTP error {response.status_code}:")
            click.echo(response.content)

    return upload_file



def watch_for_files(spooldir, uploader):
    for filename in sorted(os.listdir(os.path.join(spooldir, "new"))):
        click.echo(f"uploading {filename}")
        os.rename(
            os.path.join(spooldir, "new", filename),
            os.path.join(spooldir, "cur", filename),
        )
        try:
            uploader(os.path.join(spooldir, "cur", filename))
            os.remove(os.path.join(spooldir, "cur", filename))
        except Exception as ex:
            click.echo(f"failed to upload {filename}: {ex}")


@click.command()
@click.option("--spooldir", help="what to watch for new files to upload (standard maildir style)")
@click.option("--server", help="Nextcloud base URL, like https://www.example.com/nextcloud", default="http://thinker.local/nextcloud")
@click.option("--directory", help="Nextcloud target directory, like Photos")
@click.option("--username", help="Nextcloud username")
@click.option("--password", help="Nextcloud password", prompt=True, hide_input=True)
def cli(spooldir, server, directory, username, password):
    try:
        os.makedirs(os.path.join(spooldir, "tmp"))
    except FileExistsError:
        pass
    try:
        os.makedirs(os.path.join(spooldir, "new"))
    except FileExistsError:
        pass
    try:
        os.makedirs(os.path.join(spooldir, "cur"))
    except FileExistsError:
        pass
    watch_for_files(
        spooldir,
        nextcloud_uploader(
            username=username,
            password=password,
            server=server,
            base_path=directory,
        ),
    )


if __name__ == "__main__":
    cli()
