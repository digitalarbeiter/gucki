# Gucki

Take webcam pics in regular intervals and upload them to a Nextcloud server.

## Usage

You need a webcam and a Nextcloud account.

Run these two programs continuously:

    watchdog.py --device 0 --interval 600 --spooldir /var/spool/gucki
    nextcloud-upload.py --spooldir /var/spool/gucki --server https://www.example.com/nextcloud --username someone --password s3cr34 --directory Photos

## Notes

### OpenCV
 * [https://docs.opencv.org/master/da/df6/tutorial_py_table_of_contents_setup.html](Installing OpenCV for Python)
 * [https://docs.opencv.org/master/d6/d00/tutorial_py_root.html](OpenCV Python Tutorials)
 * [https://pypi.org/project/opencv-python/](PIP opencv-python)

### Nextcloud
 * [https://nextcloud.com/](Nextcloud homepage)
 * [https://www.howtoforge.com/tutorial/how-to-install-nextcloud-on-debian-10/](Nextcloud on Debian 10)
