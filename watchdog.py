#!/usr/bin/env python3
# coding: utf-8
import contextlib
import os
import time

import click
import cv2


@contextlib.contextmanager
def webcam(device=0):
    camera = cv2.VideoCapture(device)
    if not camera.isOpened():
        raise ValueError(f"camera {device} not open")
    try:
        yield camera
    finally:
        camera.release()


def take_pictures(device, interval, spooldir):
    while True:
        picname = time.strftime("picture_%Y%m%d-%H%M%S.png")
        with webcam(device) as camera:
            got_image, image = camera.read()
            if got_image:
                click.echo(f"picture: {picname}")
                cv2.imwrite(os.path.join(spooldir, "tmp", picname), image)
                os.rename(
                    os.path.join(spooldir, "tmp", picname),
                    os.path.join(spooldir, "new", picname),
                )
        time.sleep(interval)


@click.command()
@click.option("--device", help="webcam device index", type=int, default=0)
@click.option("--interval", help="interval between pictures (in seconds)", type=int, default=60)
@click.option("--spooldir", help="spooldir where to place pics (standard maildir setup)")
def cli(device, interval, spooldir):
    try:
        os.makedirs(os.path.join(spooldir, "tmp"))
    except FileExistsError:
        pass
    try:
        os.makedirs(os.path.join(spooldir, "new"))
    except FileExistsError:
        pass
    try:
        os.makedirs(os.path.join(spooldir, "cur"))
    except FileExistsError:
        pass
    take_pictures(device, interval, spooldir)


if __name__ == "__main__":
    cli()
